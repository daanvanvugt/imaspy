.. currentmodule:: imaspy

API reference
=============

This page provides a summary of IMASPy's API. For more details
and examples, refer to the relevant chapters in the main part of the
documentation.

IMASPy IDS manipulation
------------------------

.. autosummary::
   :toctree: generated/
   :template: custom-module-template.rst
   :recursive:

   ids_classes.IDSPrimitive
